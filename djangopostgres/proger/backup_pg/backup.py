import gzip
import shutil
import os
from datetime import datetime
import time

USER = os.environ.get('PG_USER')
DATABASE = os.environ.get('PG_DATABASE')
HOST = os.environ.get('PG_HOST')

def main():
    path = "/var/backups"
    os.mkdir(path)
    os.system('pg_dump -h '+HOST+' -U '+USER+DATABASE+' > '+path+'/psql')
    if os.path.exists(path+'/psql'):
        source_path = path+'/psql'
        destination_filename = source_path + '_' + datetime.strftime(datetime.now(), "%Y.%m.%d.%H") + '.sql.gz'
        with open(source_path, 'rb') as f_in:
            with gzip.open(destination_filename, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
                os.remove(source_path)


if __name__ == '__main__':
    main()