#!/bin/sh
#PATH=/etc:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

PGPASSWORD=postgres
export PGPASSWORD
pathB=/var/backups
dbUser=postgres
database=postgres
hostbase=172.16.104.58

find $pathB \( -name "*-1[^5].*" -o -name "*-[023]?.*" \) -ctime +3 -delete
pg_dump -h $hostbase -U $dbUser $database | gzip > $pathB/pgsql_$(date "+%Y-%m-%d").sql.gz

unset PGPASSWORD